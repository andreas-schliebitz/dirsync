#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>

#include "SyncDatabase.hpp"

SyncDatabase::SyncDatabase(const std::filesystem::path &root,
                           const std::string &machineId)
    : root(root), machineId(machineId) {
    this->dbPath = root;
    this->dbPath /= DB_FILE;

    this->lockPath = root;
    this->lockPath /= LOCK_FILE;

    this->unlock();
    this->createDb();
    this->populate();
}

void SyncDatabase::createDb() {
    while (this->locked())
        ;
    this->lock();
    if (!std::filesystem::exists(this->dbPath)) {
        FileManager::writeString(this->dbPath, "{}");
        spdlog::info("[SyncDatabase::createDb] Sync database \"{}\" created.",
                     this->dbPath.native());
    } else {
        spdlog::info("[SyncDatabase::createDb] Sync database \"{}\" found.",
                     this->dbPath.native());
    }
    this->unlock();
}

void SyncDatabase::populate() {
    while (this->locked())
        ;
    this->lock();
    this->update();
    this->commit();
    spdlog::info("[SyncDatabase::populate] Sync database \"{}\" refreshed its "
                 "state. Ready to go!",
                 this->dbPath.native());
    this->unlock();
}

// Private method, needs manual locking!
void SyncDatabase::load() {
    json dbJson;
    if (!std::filesystem::exists(this->dbPath) ||
        std::filesystem::is_empty(this->dbPath)) {
        dbJson = json::object();
    } else {
        const auto data = FileManager::readBinary(this->dbPath);
        const std::string dbStr(data.begin(), data.end());
        dbJson = json::parse(dbStr);
    }
    for (const auto &[filepath, file] : dbJson.items())
        this->db.insert({filepath, File(file)});
    spdlog::info("[SyncDatabase::load] Sync database \"{}\" loaded from disk.",
                 this->dbPath.native());
}

// Private method, needs manual locking!
void SyncDatabase::commit() {
    json dbJson;
    if (this->db.empty()) {
        dbJson = json::object();
    } else {
        for (const auto &[filepath, file] : this->db)
            dbJson[filepath] = file.toJSON();
    }

    const std::string dbStr = dbJson.dump();
    const std::vector<std::uint8_t> data(dbStr.begin(), dbStr.end());
    FileManager::writeBinary(this->dbPath, data);
    spdlog::info("[SyncDatabase::commit] Sync database \"{}\" written to disk.",
                 this->dbPath.native());
}

// Private method, needs manual locking!
void SyncDatabase::update() {
    for (const auto &entry :
         std::filesystem::recursive_directory_iterator(this->root)) {

        const auto filepath = entry.path();

        if (!entry.is_regular_file() ||
            FileManager::isReservedFile(this->root, filepath))
            continue;

        const auto relPath = FileManager::relativize(this->root, filepath);

        if (this->db.count(relPath.native()))
            continue;

        File untrackedFile(relPath);
        untrackedFile.addVersionVectorEntry(this->machineId);
        this->db.insert_or_assign(relPath, untrackedFile);
        spdlog::info(
            "[SyncDatabase::update] Added new untracked file \"{}\" to "
            "sync database \"{}\".",
            relPath.native(), this->dbPath.native());
    }
}

void SyncDatabase::lock() {
    while (this->locked())
        ;
    FileManager::writeBinary(this->lockPath, {});
    this->load();
}

void SyncDatabase::unlock() const {
    if (this->locked()) {
        FileManager::deleteFile(this->lockPath);
    } else {
        spdlog::warn("[SyncDatabase::unlock] Sync database is not locked!");
    }
}

bool SyncDatabase::locked() const {
    return std::filesystem::exists(this->lockPath);
}

File &SyncDatabase::at(const std::string &filepath) {
    while (this->locked())
        ;
    this->lock();
    spdlog::info("[SyncDatabase::at] Reading metadata for file \"{}\" from "
                 "sync database.",
                 filepath);
    auto &file = this->db.at(filepath);
    spdlog::info("[SyncDatabase::at] File \"{}\" metadata: \"{}\".", filepath,
                 file.toJSON().dump());
    this->unlock();
    return file;
}

bool SyncDatabase::contains(const std::string &filepath) {
    while (this->locked())
        ;
    this->lock();
    const bool found = this->db.count(filepath);
    spdlog::info("[SyncDatabase::contains] File \"{}\" is tracked: {}",
                 filepath, found);
    this->unlock();
    return found;
}

bool SyncDatabase::contains(const File &file) {
    return this->contains(file.native());
}

void SyncDatabase::insert(const std::string &filepath, const File &file) {
    while (this->locked())
        ;
    this->lock();
    this->db.insert_or_assign(filepath, file.metadata());
    spdlog::info(
        "[SyncDatabase::insert] Inserted \"{}\" into sync database \"{}\".",
        file.native(), this->dbPath.native());
    this->commit();
    this->unlock();
}

void SyncDatabase::erase(const std::string &filepath) {
    while (this->locked())
        ;
    this->lock();
    if (this->contains(filepath)) {
        this->db.erase(filepath);
        this->commit();
        spdlog::info(
            "[SyncDatabase::erase] Erased \"{}\" from sync database \"{}\".",
            filepath, this->dbPath.native());
    } else {
        spdlog::info("[SyncDatabase::erase] Not erasing \"{}\" from sync "
                     "database since file is not tracked.",
                     filepath);
    }
    this->unlock();
}
