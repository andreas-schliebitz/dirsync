#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>
#include "VersionVector.hpp"

VersionVector::VersionVector() : vec({}) {}

VersionVector::VersionVector(
    const std::unordered_map<std::string, std::size_t> &vec)
    : vec(vec) {}

void VersionVector::increment(const std::string &machineId) {
    if (this->contains(machineId))
        this->vec.at(machineId) += 1;
}

void VersionVector::insertMax(const std::string &thisMachineId,
                              const std::string &otherMachineId,
                              VersionVector &other) {
    if (!this->contains(thisMachineId) || !other.contains(otherMachineId))
        return;
    const auto maxCtr =
        std::max(this->vec.at(thisMachineId), other.vec.at(otherMachineId));
    this->set(thisMachineId, maxCtr);
    other.set(otherMachineId, maxCtr);
}

bool VersionVector::contains(const std::string &machineId) const {
    return (this->vec.count(machineId) > 0);
}

void VersionVector::set(const std::string &machineId, std::size_t ctr) {
    if (this->contains(machineId))
        this->vec.at(machineId) = ctr;
}

void VersionVector::insert(const std::string &machineId) {
    if (!this->contains(machineId))
        this->vec.insert({machineId, 0});
}

void VersionVector::clear() { this->vec.clear(); }

const std::unordered_map<std::string, std::size_t> &
VersionVector::data() const {
    return this->vec;
}

std::size_t VersionVector::at(const std::string &machineId) const {
    return this->vec.at(machineId);
}

std::size_t VersionVector::size() const { return this->vec.size(); }

std::vector<std::string> VersionVector::machineIds() const {
    std::vector<std::string> keys;
    keys.reserve(this->size());

    for (auto const &entry : this->vec)
        keys.push_back(entry.first);
    return keys;
}

bool VersionVector::operator==(const VersionVector &rhs) const {
    const auto &lhs = *this;
    const auto machineIds =
        (lhs.size() >= rhs.size()) ? lhs.machineIds() : rhs.machineIds();
    for (const auto &machineId : machineIds) {
        if (!lhs.vec.count(machineId) || !rhs.vec.count(machineId) ||
            lhs.vec.at(machineId) != rhs.vec.at(machineId))
            return false;
    }
    return true;
}

bool operator<(const VersionVector &lhs, const VersionVector &rhs) {
    const auto machineIds =
        (lhs.size() >= rhs.size()) ? lhs.machineIds() : rhs.machineIds();

    const auto allLessOrEqual = [&machineIds, &lhs, &rhs]() -> bool {
        for (const auto &machineId : machineIds) {
            if (!lhs.vec.count(machineId) || !rhs.vec.count(machineId)) {
                if (lhs.vec.count(machineId))
                    return false;
            } else if (lhs.vec.at(machineId) > rhs.vec.at(machineId)) {
                return false;
            }
        }
        return true;
    };

    const auto atLeastOneStrictlyLess = [&machineIds, &lhs, &rhs]() -> bool {
        for (const auto &machineId : machineIds) {
            if (!lhs.vec.count(machineId) || !rhs.vec.count(machineId)) {
                if (!lhs.vec.count(machineId))
                    return true;
            } else if (lhs.vec.at(machineId) < rhs.vec.at(machineId)) {
                return true;
            }
        }
        return false;
    };

    return allLessOrEqual() && atLeastOneStrictlyLess();
}
