#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>
#include <cerrno>
#include <cstdio>
#include <fcntl.h>

#include "FileManager.hpp"

void FileManager::createFile(const std::filesystem::path &dst) {
    std::ofstream out(dst, std::ios::out | std::ios::trunc);
    out.close();
}

std::vector<std::uint8_t>
FileManager::readBinary(const std::filesystem::path &filepath) {
    std::ifstream infile(filepath, std::ios::in | std::ios::binary);
    return std::vector<std::uint8_t>(std::istreambuf_iterator<char>(infile),
                                     std::istreambuf_iterator<char>());
}

File FileManager::readFile(const std::filesystem::path &filepath) {
    return File(filepath, FileManager::readBinary(filepath));
}

void FileManager::writeBinary(const std::filesystem::path &dst,
                              const std::vector<std::uint8_t> &data) {
    std::fstream outfile(dst, std::ios::out | std::ios::binary);
    outfile.write(reinterpret_cast<const char *>(data.data()), data.size());
    outfile.close();
}

void FileManager::writeString(const std::filesystem::path &dst,
                              const std::string &content) {
    std::ofstream out(dst, std::ios::out | std::ios::trunc);
    out << content;
    out.close();
}

std::string FileManager::readString(const std::filesystem::path &filepath) {
    std::ifstream ifs(filepath);
    std::stringstream ss;
    ss << ifs.rdbuf();
    ifs.close();
    return ss.str();
}

void FileManager::writeFile(const std::filesystem::path &dst,
                            const File &file) {
    FileManager::writeBinary(dst, file.getData());
}

bool FileManager::isOpen(const std::filesystem::path &filepath) {
    if (!std::filesystem::exists(filepath))
        return false;

    const int fd = open(filepath.c_str(), O_RDONLY);
    if (fd < 0) {
        spdlog::error(
            "[FileManager::isOpen] Error checking if \"{}\" is "
            "currently in use. To avoid potential data loss, true is assumed.",
            filepath.native());
        return true;
    }

    bool inUse;

    if (fcntl(fd, F_SETLEASE, F_WRLCK) && EAGAIN == errno) {
        spdlog::warn("[FileManager::isOpen] File \"{}\" is currently in use.",
                     filepath.native());
        inUse = true;
    } else {
        fcntl(fd, F_SETLEASE, F_UNLCK);
        spdlog::info("[FileManager::isOpen] File \"{}\" is currently unused.",
                     filepath.native());
        inUse = false;
    }

    close(fd);
    return inUse;
}

bool FileManager::isOpen(const std::filesystem::path &root, const File &file) {
    std::filesystem::path src = root;
    src += file.getFilepath();
    return FileManager::isOpen(src);
}

void FileManager::deleteFile(const std::filesystem::path &filepath) {
    if (!std::filesystem::exists(filepath)) {
        spdlog::warn(
            "[FileManager::deleteFile] Cannot delete non-existing file \"{}\".",
            filepath.native());
        return;
    }

    if (std::remove(filepath.c_str()) == 0) {
        spdlog::info("[FileManager::deleteFile] File \"{}\" deleted.",
                     filepath.native());
    } else {
        spdlog::error("[FileManager::deleteFile] Error deleting file \"{}\".",
                      filepath.native());
    }
}

std::filesystem::path
FileManager::relativize(const std::filesystem::path &root,
                        const std::filesystem::path &absPath) {
    const auto &rootStr = root.native();
    const auto &absPathStr = absPath.native();
    std::string relativeToRoot{};
    std::set_difference(absPathStr.begin(), absPathStr.end(), rootStr.begin(),
                        rootStr.end(), std::back_inserter(relativeToRoot));
    return relativeToRoot;
}

bool FileManager::isReservedFile(const std::filesystem::path &root,
                                 const std::filesystem::path &toCheck) {
    for (const auto &reservedFilename : {DB_FILE, LOCK_FILE, LAST_RECEIVED}) {
        std::filesystem::path reservedFilepath = root;
        reservedFilepath /= reservedFilename;
        if (reservedFilepath == toCheck)
            return true;
    }
    return false;
}
