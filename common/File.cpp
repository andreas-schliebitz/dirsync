#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>
#include <cassert>

#include "File.hpp"

File::File() : data({}), size(0) {}

File::File(const std::filesystem::path &filepath,
           const std::vector<std::uint8_t> &data)
    : filepath(filepath), data(data), size(data.size()) {}

File::File(const std::filesystem::path &filepath)
    : filepath(filepath), data({}), size(0) {}

File::File(const json &j)
    : filepath(std::filesystem::path(j["filepath"].get<std::string>())),
      data(j["data"]), size(j["size"]), status(j["status"]),
      versionVector(j["versionVector"]) {}

const std::filesystem::path &File::getFilepath() const {
    return this->filepath;
}

const std::vector<std::uint8_t> &File::getData() const { return this->data; }

std::size_t File::getSize() const { return this->size; }

const FileStatus &File::getStatus() const { return this->status; }

VersionVector &File::getVersionVector() { return this->versionVector; }

const std::string &File::native() const { return this->filepath.native(); }

std::filesystem::path File::filename() const {
    return this->filepath.filename();
}

std::filesystem::path File::parentPath() const {
    return this->filepath.parent_path();
}

std::filesystem::path File::stem() const { return this->filepath.stem(); }

std::filesystem::path File::extension() const {
    return this->filepath.extension();
}

const std::uint8_t *File::bytes() const { return this->data.data(); }

bool File::empty() const { return this->size == 0 && this->isset(); }

bool File::isset() const { return !this->filepath.empty(); }

void File::setFilepath(const std::filesystem::path &filepath) {
    assert(!filepath.empty());
    this->filepath = filepath;
}

void File::addStatus(const FileStatus &status) {
    assert(!this->filepath.empty());
    this->status = status;
}

void File::addData(const std::vector<std::uint8_t> &data) {
    assert(!this->filepath.empty());
    if (this->size != 0)
        assert(this->size == data.size());
    this->data = data;
    this->size = data.size();
}

void File::addSize(std::size_t size) {
    assert(!this->filepath.empty() && this->size == 0);
    if (!this->data.empty())
        assert(this->data.size() == size);
    this->size = size;
}

void File::addVersionVector(const VersionVector &vec) {
    assert(this->versionVector.data().empty());
    this->versionVector = vec;
}

void File::incrementVersionVector(const std::string &machineId) {
    this->addVersionVectorEntry(machineId);
    this->versionVector.increment(machineId);
}

void File::clearVersionVector() { this->versionVector.clear(); }

void File::addVersionVectorEntry(const std::string &machineId) {
    // .insert() only inserts if key is NOT present.
    this->versionVector.insert({machineId, 0});
}

File File::metadata() const {
    File meta(this->filepath);
    meta.addVersionVector(this->versionVector);
    meta.addSize(this->size);
    meta.addStatus(this->status);
    return meta;
}

json File::toJSON() const {
    const json jsonMsg = {{"type", "file"},
                          {"filepath", this->native()},
                          {"size", this->size},
                          {"status", this->status},
                          {"versionVector", this->versionVector.data()},
                          {"data", this->data}};
    return jsonMsg;
}

std::vector<std::uint8_t> File::toBSON() const {
    return json::to_bson(this->toJSON());
}
