#ifndef USING_HPP
#define USING_HPP

#include "client_wss.hpp"
#include "json.hpp"
#include "server_wss.hpp"

using WssServer = SimpleWeb::SocketServer<SimpleWeb::WSS>;
using SrvConnectionPtr = std::shared_ptr<WssServer::Connection>;
using SrvInMessagePtr = std::shared_ptr<WssServer::InMessage>;

using WssClient = SimpleWeb::SocketClient<SimpleWeb::WSS>;
using CltConnectionPtr = std::shared_ptr<WssClient::Connection>;
using CltInMessagePtr = std::shared_ptr<WssClient::InMessage>;

using ErrorCode = SimpleWeb::error_code;
using json = nlohmann::json;

constexpr auto DB_FILE = ".sync.db";
constexpr auto LOCK_FILE = ".sync.db.lock";
constexpr auto LAST_RECEIVED = ".received";

#endif /* USING_HPP */
