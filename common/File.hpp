#ifndef FILE_HPP
#define FILE_HPP

#include <vector>
#include <filesystem>
#include <iostream>

#include "Using.hpp"
#include "VersionVector.hpp"
#include "FileStatus.hpp"

class File {
  public:
    File();
    File(const std::filesystem::path &filepath,
         const std::vector<std::uint8_t> &data);
    File(const std::filesystem::path &filepath);
    File(const json &j);

    // Getter
    const std::filesystem::path &getFilepath() const;
    const std::vector<std::uint8_t> &getData() const;
    std::size_t getSize() const;
    const FileStatus &getStatus() const;
    VersionVector &getVersionVector();

    const std::string &native() const;
    std::filesystem::path filename() const;
    std::filesystem::path parentPath() const;
    std::filesystem::path stem() const;
    std::filesystem::path extension() const;
    const std::uint8_t *bytes() const;
    bool empty() const;
    bool isset() const;

    void setFilepath(const std::filesystem::path &filepath);
    void addStatus(const FileStatus &status);
    void addData(const std::vector<std::uint8_t> &data);
    void addSize(std::size_t size);
    void addVersionVector(const VersionVector &vec);

    std::vector<std::uint8_t> toBSON() const;
    json toJSON() const;

    void incrementVersionVector(const std::string &machineId);
    void addVersionVectorEntry(const std::string &machineId);
    void clearVersionVector();
    File metadata() const;

  private:
    std::filesystem::path filepath;
    std::vector<std::uint8_t> data;
    std::size_t size;
    FileStatus status;
    VersionVector versionVector;
};

#endif /* FILE_HPP */
