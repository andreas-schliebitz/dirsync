#ifndef FILESTATUS_HPP
#define FILESTATUS_HPP

enum class FileStatus { CREATED, MODIFIED, ERASED };

#endif /* FILESTATUS_HPP */
