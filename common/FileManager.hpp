#ifndef FILEMANAGER_HPP
#define FILEMANAGER_HPP

#include <filesystem>
#include <fstream>
#include <iostream>
#include <ios>
#include <sstream>
#include <vector>

#include "Using.hpp"
#include "File.hpp"

class FileManager {
  public:
    FileManager() = delete;
    FileManager(const FileManager &) = delete;

    static void createFile(const std::filesystem::path &dst);
    static std::vector<std::uint8_t>
    readBinary(const std::filesystem::path &filepath);
    static File readFile(const std::filesystem::path &filepath);
    static std::string readString(const std::filesystem::path &filepath);
    static void writeBinary(const std::filesystem::path &dst,
                            const std::vector<std::uint8_t> &data);
    static void writeString(const std::filesystem::path &dst,
                            const std::string &content);
    static void writeFile(const std::filesystem::path &dst, const File &file);
    static bool isOpen(const std::filesystem::path &filepath);
    static bool isOpen(const std::filesystem::path &root, const File &file);
    static void deleteFile(const std::filesystem::path &filepath);
    static std::filesystem::path
    relativize(const std::filesystem::path &root,
               const std::filesystem::path &absPath);
    static bool isReservedFile(const std::filesystem::path &root,
                               const std::filesystem::path &toCheck);
};

#endif /* FILEMANAGER_HPP */
