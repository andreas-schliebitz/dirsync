#ifndef VERSIONVECTOR_HPP
#define VERSIONVECTOR_HPP

#include <unordered_map>
#include <string>
#include <cassert>
#include <vector>

class VersionVector {
  public:
    VersionVector();
    VersionVector(const std::unordered_map<std::string, std::size_t> &vec);

    void increment(const std::string &machineId);
    void insertMax(const std::string &thisMachineId,
                   const std::string &otherMachineId, VersionVector &other);
    bool contains(const std::string &machineId) const;
    void insert(const std::string &machineId);
    void clear();
    void set(const std::string &machineId, std::size_t ctr);
    std::size_t at(const std::string &machineId) const;
    const std::unordered_map<std::string, std::size_t> &data() const;
    std::size_t size() const;
    std::vector<std::string> machineIds() const;

    bool operator==(const VersionVector &rhs) const;
    friend bool operator<(const VersionVector &lhs, const VersionVector &rhs);

  private:
    std::unordered_map<std::string, std::size_t> vec;
};

#endif /* VERSIONVECTOR_HPP */
