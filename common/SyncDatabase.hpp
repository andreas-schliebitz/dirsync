#ifndef SYNCDATABASE_HPP
#define SYNCDATABASE_HPP

#include <filesystem>
#include <string>
#include <unordered_map>

#include "Using.hpp"
#include "FileManager.hpp"
#include "File.hpp"

class SyncDatabase {
public:
  SyncDatabase(const std::filesystem::path &root, const std::string &machineId);

  void createDb();
  void lock();
  void unlock() const;
  bool locked() const;
  File &at(const std::string &filepath);
  void erase(const std::string &filepath);
  bool contains(const std::string &filepath);
  bool contains(const File &file);
  void insert(const std::string &filepath, const File &file);
  void populate();

private:
  void update();
  void load();
  void commit();

  std::filesystem::path root;
  std::string machineId;
  std::filesystem::path dbPath;
  std::filesystem::path lockPath;
  std::unordered_map<std::string, File> db;
};

#endif /* SYNCDATABASE_HPP */
