#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>

#include "DirSyncClientServer.hpp"

DirSyncClientServer::DirSyncClientServer(const std::filesystem::path &root,
                                         std::uint16_t port,
                                         const std::string &endpoint,
                                         const std::string &crtPath,
                                         const std::string &keyPath)
    : root(root), port(port), endpoint(endpoint),
      machineId(machineid::machineHash()), crtPath(crtPath), keyPath(keyPath),
      db(root, this->machineId) {

    spdlog::info(
        "[DirSyncClientServer::DirSyncClientServer] DirSyncClientServer "
        "listening on "
        "port {} with endpoint \"{}\", root \"{}\", machineId \"{}\" crtPath "
        "\"{}\", keyPath \"{}\".",
        port, endpoint, root.native(), machineId, crtPath, keyPath);

    this->server = std::unique_ptr<WssServer>(new WssServer(crtPath, keyPath));
    this->server->config.port = port;

    this->onOpen();
    this->onClose();
    this->onError();
    this->onHandshake();
    this->onMessage();
}

const std::filesystem::path &DirSyncClientServer::getRoot() const {
    return this->root;
}

std::uint16_t DirSyncClientServer::getPort() const { return this->port; }

const std::string &DirSyncClientServer::getEndpoint() const {
    return this->endpoint;
}

const std::string &DirSyncClientServer::getCrtPath() const {
    return this->crtPath;
}

const std::string &DirSyncClientServer::getKeyPath() const {
    return this->keyPath;
}

void DirSyncClientServer::setPort(std::uint16_t port) { this->port = port; }

void DirSyncClientServer::setEndpoint(const std::string &endpoint) {
    this->endpoint = endpoint;
}

void DirSyncClientServer::setCrtPath(const std::string &crtPath) {
    this->crtPath = crtPath;
}

void DirSyncClientServer::setKeyPath(const std::string &keyPath) {
    this->keyPath = keyPath;
}

void DirSyncClientServer::start() {
    std::promise<unsigned short> server_port;
    std::thread clientServerThread([this, &server_port]() {
        this->server->start([&server_port](unsigned short port) {
            server_port.set_value(port);
        });
    });
    spdlog::info("[DirSyncClientServer::start] Client server thread started.");
    clientServerThread.join();
}

void DirSyncClientServer::onMessage() {
    this->server->endpoint[this->endpoint]
        .on_message = [this]([[maybe_unused]] SrvConnectionPtr con,
                             SrvInMessagePtr inMsg) {
        spdlog::info("[DirSyncClientServer::onMessage] On message callback "
                     "triggered. Incoming message from server...");
        switch (inMsg->fin_rsv_opcode) {
        // Incoming message is binary data
        case 130: {
            spdlog::info(
                "[DirSyncClientServer::onMessage] Received binary message.");
            this->handleBinaryMessage(inMsg->binary());
            break;
        }
        // Incoming message is not supported
        default:
            spdlog::info(
                "[DirSyncClientServer::onMessage] Received message with "
                "unsupported opcode {}",
                inMsg->fin_rsv_opcode);
            break;
        }
    };
}

void DirSyncClientServer::onOpen() {
    this->server->endpoint[this->endpoint]
        .on_open = []([[maybe_unused]] SrvConnectionPtr con) {
        spdlog::info(
            "[DirSyncClientServer::onOpen] Client server opened connection.");
    };
}

void DirSyncClientServer::onClose() {
    this->server->endpoint[this->endpoint].on_close =
        []([[maybe_unused]] SrvConnectionPtr con, int status,
           const std::string &reason) {
            spdlog::info("[DirSyncClientServer::onClose] Client server closed "
                         "connection with status code {}.\n\t "
                         "Reason: {}",
                         status, reason);
        };
}
void DirSyncClientServer::onError() {
    this->server->endpoint[this->endpoint].on_error =
        []([[maybe_unused]] SrvConnectionPtr con, const ErrorCode &ec) {
            spdlog::error(
                "[DirSyncClientServer::onError] Error {}.\n\tError message: {}",
                ec.value(), ec.message());
        };
}

void DirSyncClientServer::onHandshake() {
    this->server->endpoint[this->endpoint].on_handshake =
        []([[maybe_unused]] SrvConnectionPtr con,
           [[maybe_unused]] SimpleWeb::CaseInsensitiveMultimap
               &responseHeader) {
            spdlog::info("[DirSyncClientServer::onHandshake] Client server "
                         "responds with switching protocols.");
            return SimpleWeb::StatusCode::information_switching_protocols;
        };
}

void DirSyncClientServer::handleBinaryMessage(
    const std::vector<std::uint8_t> &bsonData) {
    const json jsonMsg = json::from_bson(bsonData);
    const auto &type = jsonMsg["type"].get<std::string>();

    spdlog::info(
        "[DirSyncClientServer::handleBinaryMessage] Received BSON message "
        "of type {}: {}",
        type, jsonMsg.dump());

    if (type == "file") {
        File file(jsonMsg);
        file.addVersionVectorEntry(this->machineId);

        if (!this->db.contains(file)) {
            spdlog::info("[DirSyncClientServer::handleModifiedFile] Received "
                         "file \"{}\" was not tracked. Accepting changes.",
                         file.native());
            FileManager::isOpen(this->root, file)
                ? this->write(this->createConflictingFileCopy(file))
                : this->write(file);
            return;
        }

        const auto &receivedMachineId = jsonMsg["machineId"];

        File &localFile = this->db.at(file.native());

        switch (file.getStatus()) {
        case FileStatus::MODIFIED: {
            this->handleIncomingModifiedFile(receivedMachineId, localFile,
                                             file);
            break;
        }
        case FileStatus::CREATED: {
            this->handleIncomingCreatedFile(file);
            break;
        }
        case FileStatus::ERASED: {
            this->handleIncomingErasedFile(file);
            break;
        }
        default:
            // Never reached
            break;
        }
    }
}

void DirSyncClientServer::handleIncomingModifiedFile(
    const std::string &receivedMachineId, File &localFile, File &file) {
    const VersionStatus receivedVersionStatus =
        DirSyncClientServer::compareVersionVectors(
            file.getVersionVector(), localFile.getVersionVector());

    switch (receivedVersionStatus) {
    case VersionStatus::IDENTICAL: {
        spdlog::info("[DirSyncClientServer::handleModifiedFile] Received "
                     "file \"{}\" is identical with local copy. "
                     "Doing nothing.",
                     file.native());
        break;
    }
    case VersionStatus::MODIFIED_AFTER: {
        spdlog::info("[DirSyncClientServer::handleModifiedFile] Received "
                     "file \"{}\" was modified AFTER local copy. "
                     "Accepting changes.",
                     file.native());
        this->insertMaxCounter(receivedMachineId, localFile.getVersionVector(),
                               file.getVersionVector());
        FileManager::isOpen(this->root, file)
            ? this->write(this->createConflictingFileCopy(file))
            : this->write(file);
        break;
    }
    case VersionStatus::MODIFIED_BEFORE: {
        spdlog::warn("[DirSyncClientServer::handleModifiedFile] Received "
                     "file \"{}\" was modified BEFORE local copy. "
                     "Rejecting changes.",
                     file.native());
        break;
    }
    case VersionStatus::CONCURRENT: {
        spdlog::error("[DirSyncClientServer::handleModifiedFile] Received "
                      "file \"{}\" and local copy were modified "
                      "concurrently. "
                      "Resolving conflict by redundant copy.",
                      file.native());
        this->write(this->createConflictingFileCopy(file));
        break;
    }
    default:
        // Never reached
        break;
    }
}
void DirSyncClientServer::handleIncomingCreatedFile(const File &file) {
    spdlog::info("[DirSyncClientServer::handleCreatedFile] Received "
                 "file \"{}\" was newly created. Accepting changes.",
                 file.native());

    FileManager::isOpen(this->root, file)
        ? this->write(this->createConflictingFileCopy(file))
        : this->write(file);
}

void DirSyncClientServer::handleIncomingErasedFile(const File &file) {
    spdlog::info("[DirSyncClientServer::handleErasedFile] Received "
                 "file \"{}\" was erased. Also erasing.",
                 file.native());
    this->erase(file);
}

void DirSyncClientServer::insertMaxCounter(const std::string &receivedMachineId,
                                           VersionVector &local,
                                           VersionVector &received) {
    spdlog::info("[DirSyncClientServer::insertMaxCounter] Setting max({}, {}) "
                 "for both version vectors.",
                 local.at(this->machineId), received.at(receivedMachineId));

    local.insertMax(this->machineId, receivedMachineId, received);
}

File DirSyncClientServer::createConflictingFileCopy(
    const File &conflictingFile) const {
    File file(conflictingFile);
    std::filesystem::path dst = file.getFilepath().parent_path();
    dst /= file.stem();
    dst += " (conflicting copy " + DirSyncClientServer::timestamp() + ")";
    dst += file.extension();
    file.setFilepath(dst);
    file.clearVersionVector();
    file.addVersionVectorEntry(this->machineId);
    return file;
}

void DirSyncClientServer::write(const File &file) {
    std::filesystem::path dst = this->root;
    dst += file.getFilepath();
    FileManager::writeFile(dst, file);
    this->writeLastReceived(file.getFilepath());
    this->db.insert(file.native(), file);
}

void DirSyncClientServer::writeLastReceived(
    const std::filesystem::path &filepath) const {
    std::filesystem::path dst = this->root;
    dst /= LAST_RECEIVED;
    FileManager::writeString(dst, filepath);
}

void DirSyncClientServer::createLastReceived() const {
    std::filesystem::path dst = this->root;
    dst /= LAST_RECEIVED;
    FileManager::createFile(dst);
}

void DirSyncClientServer::erase(const File &file) {
    std::filesystem::path dst = this->root;
    dst /= file.getFilepath();

    if (FileManager::isOpen(dst)) {
        spdlog::error(
            "[DirSyncClientServer::erase] Cannot erase opened file \"{}\".",
            dst.native());
        return;
    }

    FileManager::deleteFile(dst);
    this->db.erase(file.native());
    spdlog::error("[DirSyncClientServer::erase] File \"{}\" erased.",
                  dst.native());
}

VersionStatus
DirSyncClientServer::compareVersionVectors(const VersionVector &received,
                                           const VersionVector &local) {
    if (received == local) {
        return VersionStatus::IDENTICAL;
    } else if (received < local) {
        return VersionStatus::MODIFIED_BEFORE;
    } else if (local < received) {
        return VersionStatus::MODIFIED_AFTER;
    } else {
        return VersionStatus::CONCURRENT;
    }
}

std::string DirSyncClientServer::timestamp() {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    std::ostringstream oss;
    oss << std::put_time(&tm, "%Y-%m-%d %H:%M:%S");
    return oss.str();
}
