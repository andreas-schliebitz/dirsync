#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>

#include <iostream>
#include <filesystem>

#include "getopt_pp.h"
#include "main.hpp"

int main(int argc, char **argv) {
    GetOpt::GetOpt_pp ops(argc, argv);

    bool help;
    ops >> GetOpt::OptionPresent('h', "help", help);
    if (help) {
        printUsage();
        std::exit(EXIT_SUCCESS);
    }

    std::string root, endpoint, crtPath, keyPath;
    std::uint16_t port = 8081;

    const std::string pwd = std::filesystem::current_path().native();

    ops >> GetOpt::Option('r', "root", root, pwd + "/synced");

    std::filesystem::path fsRoot(root);
    if (fsRoot.is_relative()) {
        spdlog::error("[main] Provided root path \"{}\" has to be absolute.",
                      root);
        std::exit(EXIT_FAILURE);
    }

    ops >> GetOpt::Option('p', "port", port);
    ops >> GetOpt::Option('e', "endpoint", endpoint, "^/file/?$");
    ops >> GetOpt::Option('c', "crtPath", crtPath, pwd + "/server.crt");
    ops >> GetOpt::Option('k', "keyPath", keyPath, pwd + "/server.key");

    DirSyncClientServer clientServer(fsRoot, port, endpoint, crtPath, keyPath);
    clientServer.start();

    return EXIT_SUCCESS;
}

void printUsage() {
    std::cout
        << "Usage:\n\t./dirsyncclientserver -h|--help\n\t./dirsyncclientserver "
           "-r|--root -p|--port -e|--endpoint "
           "-c|--crtPath -k|--keyPath\n\t\t-r|--root: Filesystem path to "
           "receive synced files.\n\t\t-p|--port: Client server's "
           "port.\n\t\t-e|--endpoint: Client server's endpoint to listen "
           "on.\n\t\t-c|--crtPath: Path to client server's "
           "certificate.\n\t\t-k|--keyPath: Path to client server's key."
        << '\n';
}
