#ifndef DIRSYNCCLIENTSERVER_HPP
#define DIRSYNCCLIENTSERVER_HPP

#include <cstdlib>
#include <string>
#include <memory>
#include <filesystem>
#include <vector>
#include <ctime>
#include <iomanip>
#include <sstream>

#include "Using.hpp"
#include "machineid.h"
#include "File.hpp"
#include "VersionVector.hpp"
#include "FileManager.hpp"
#include "VersionStatus.hpp"
#include "FileStatus.hpp"
#include "SyncDatabase.hpp"

class DirSyncClientServer {
  public:
    DirSyncClientServer(const std::filesystem::path &root,
                        std::uint16_t port = 8081,
                        const std::string &endpoint = "/",
                        const std::string &crtPath = "./server.crt",
                        const std::string &keyPath = "./server.key");

    // Getter
    const std::filesystem::path &getRoot() const;
    std::uint16_t getPort() const;
    const std::string &getEndpoint() const;
    const std::string &getCrtPath() const;
    const std::string &getKeyPath() const;

    // Setter
    void setPort(std::uint16_t port);
    void setEndpoint(const std::string &endpoint);
    void setCrtPath(const std::string &crtPath);
    void setKeyPath(const std::string &keyPath);

    void start();

  private:
    std::filesystem::path root;
    std::uint16_t port;
    std::string endpoint;
    std::string machineId;
    std::string crtPath;
    std::string keyPath;
    SyncDatabase db;

    std::unique_ptr<WssServer> server;

    // Listener
    void onMessage();
    void onOpen();
    void onClose();
    void onError();
    void onHandshake();

    // Message handlers
    void handleBinaryMessage(const std::vector<std::uint8_t> &bsonData);

    // Incoming file handlers
    void handleIncomingModifiedFile(const std::string &receivedMachineId,
                                    File &localFile, File &file);
    void handleIncomingCreatedFile(const File &file);
    void handleIncomingErasedFile(const File &file);

    void insertMaxCounter(const std::string &receivedMachineId,
                          VersionVector &local, VersionVector &received);

    void write(const File &file);
    void erase(const File &file);

    void createLastReceived() const;
    void writeLastReceived(const std::filesystem::path &filepath) const;
    File createConflictingFileCopy(const File &conflictingFile) const;

    static VersionStatus compareVersionVectors(const VersionVector &received,
                                               const VersionVector &local);
    static std::string timestamp();
};

#endif /* DIRSYNCCLIENTSERVER_HPP */
