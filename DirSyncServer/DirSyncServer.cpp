#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>
#include "DirSyncServer.hpp"

DirSyncServer::DirSyncServer(std::uint16_t port,
                             const std::string &store,
                             const std::string &crtPath,
                             const std::string &keyPath,
                             const std::string &endpoint)
    : port(port), store(store), crtPath(crtPath),
      keyPath(keyPath), endpoint(endpoint) {
    this->server = std::unique_ptr<WssServer>(new WssServer(crtPath, keyPath));
    this->server->config.port = port;

    this->onOpen();
    this->onClose();
    this->onError();
    this->onHandshake();
    this->onMessage();

    spdlog::info("[DirSyncServer::DirSyncServer] DirSync listening on "
                 "port {} with endpoint \"{}\", store \"{}\", crtPath "
                 "\"{}\", keyPath \"{}\".",
                 port, endpoint, store, crtPath, keyPath);
}

// Getter
const std::filesystem::path &DirSyncServer::getStore() const {
    return this->store;
}

const std::filesystem::path &DirSyncServer::getCrtPath() const {
    return this->crtPath;
}

const std::filesystem::path &DirSyncServer::getKeyPath() const {
    return this->keyPath;
}

std::uint16_t DirSyncServer::getPort() const { return this->port; }

// Setter
void DirSyncServer::setStore(
    const std::filesystem::path &store) {
    this->store = store;
}

void DirSyncServer::setCrtPath(const std::filesystem::path &crtPath) {
    this->crtPath = crtPath;
}

void DirSyncServer::setKeyPath(const std::filesystem::path &keyPath) {
    this->keyPath = keyPath;
}

void DirSyncServer::setPort(std::uint16_t port) { this->port = port; }

void DirSyncServer::handleStringMessage(const std::string &strMsg) {
    const json jsonMsg = json::parse(strMsg);
    const auto &type = jsonMsg["type"].get<std::string>();

    if (type == "registration") {
        this->handleRegistration(jsonMsg);
    }
}

void DirSyncServer::handleRegistration(const json &jsonMsg) {
    const auto &machineId = jsonMsg["machineId"];
    const std::string conStr = jsonMsg["ip"].get<std::string>() + ":" +
                               std::to_string(jsonMsg["port"].get<int>()) +
                               jsonMsg["endpoint"].get<std::string>();
    spdlog::info("[DirSyncServer::handleRegistration] Registration of client "
                 "server received: {}, {}",
                 machineId, conStr);
    this->connections.insert_or_assign(machineId, conStr);
}

void DirSyncServer::handleBinaryMessage(
    const std::vector<std::uint8_t> &bsonData) const {
    const json jsonMsg = json::from_bson(bsonData);
    const auto &type = jsonMsg["type"].get<std::string>();

    if (type == "file") {
        const auto &machineId = jsonMsg["machineId"];
        spdlog::info("[DirSyncServer::handleFileData] Received JSON message "
                     "of type \"file\" from \"{}\": {}.",
                     machineId, jsonMsg.dump());
        const File file(jsonMsg);

        this->writeFileData(file);
        this->distributeFileData(machineId, bsonData);
    }
}

void DirSyncServer::writeFileData(const File &file) const {
    std::filesystem::path writeFilePath = this->store;
    writeFilePath /= file.filename();
    FileManager::writeFile(writeFilePath, file);
    spdlog::info(
        "[DirSyncServer::writeFileData] Received file was written to: {}",
        writeFilePath.native());
}

void DirSyncServer::distributeFileData(
    const std::string &machineId,
    const std::vector<std::uint8_t> &bsonData) const {
    for (const auto &[poolMachineId, poolConStr] : this->connections) {
        if (poolMachineId == machineId) {
            spdlog::info("[DirSyncServer::distributeFileData] Skipping "
                         "connection of original sender \"{}\" ({}).",
                         machineId, poolConStr);
            continue;
        }

        spdlog::info("[DirSyncServer::distributeFileData] Forwarding BSON "
                     "message to connected client \"{}\" ({}).",
                     poolMachineId, poolConStr);

        [&poolConStr, &bsonData]() -> void {
            const std::unique_ptr<WssClient> client(
                new WssClient(poolConStr, false));
            client->on_open = [&poolConStr, &bsonData](CltConnectionPtr con) {
                spdlog::info(
                    "[DirSyncServer::distribute::onOpen] Connection to client "
                    "server opened: \"{}\"",
                    poolConStr);
                con->send(bsonData);
                con->send_close(1000,
                                "Server sent file to " + poolConStr + ".");
            };

            client->on_close = []([[maybe_unused]] CltConnectionPtr con,
                                  int status, const std::string &reason) {
                spdlog::info("[DirSyncServer::distribute::onClose] Connection "
                             "to client server "
                             "closed with status "
                             "code {}.\n\tReason: {}",
                             status, reason);
            };

            client->on_error = []([[maybe_unused]] CltConnectionPtr con,
                                  const ErrorCode &ec) {
                spdlog::error("[DirSyncServer::distribute::onError] Error "
                              "{}.\n\tError message: {}",
                              ec.value(), ec.message());
            };
            client->start();
        }();
    }
}

void DirSyncServer::onMessage() {
    this->server->endpoint[this->endpoint].on_message =
        [this]([[maybe_unused]] SrvConnectionPtr con, SrvInMessagePtr inMsg) {
            spdlog::info("[DirSyncServer::onMessage] Received opcode {}",
                         inMsg->fin_rsv_opcode);
            switch (inMsg->fin_rsv_opcode) {
            case 129: {
                spdlog::info(
                    "[DirSyncServer::onMessage] Received string message.");
                this->handleStringMessage(inMsg->string());
                break;
            }
            case 130: {
                spdlog::info(
                    "[DirSyncServer::onMessage] Received binary message.");
                this->handleBinaryMessage(inMsg->binary());
                break;
            }
            default:
                spdlog::info("[DirSyncServer::onMessage] Received unsupported "
                             "message type.");
                break;
            }
        };
}

void DirSyncServer::onOpen() {
    this->server->endpoint[this->endpoint].on_open =
        []([[maybe_unused]] SrvConnectionPtr con) {
            spdlog::info(
                "[DirSyncServer::onOpen] Opened connection to client.");
        };
}

void DirSyncServer::onClose() {
    this->server->endpoint[this->endpoint].on_close =
        []([[maybe_unused]] SrvConnectionPtr con, int status,
           [[maybe_unused]] const std::string &reason) {
            spdlog::info("[DirSyncServer::onClose] Closed connection to "
                         "client. Status code: {}\n\tReason: {}.",
                         status, reason);
        };
}

void DirSyncServer::onError() {
    this->server->endpoint[this->endpoint].on_error =
        []([[maybe_unused]] SrvConnectionPtr con, const ErrorCode &ec) {
            spdlog::error(
                "[DirSyncServer::onError] Error {}.\n\tError message: {}",
                ec.value(), ec.message());
        };
}

void DirSyncServer::onHandshake() {
    this->server->endpoint[this->endpoint].on_handshake =
        []([[maybe_unused]] SrvConnectionPtr con,
           [[maybe_unused]] SimpleWeb::CaseInsensitiveMultimap
               &response_header) {
            spdlog::info("[DirSyncServer::onHandshake] Responding with "
                         "switching protocols.");
            return SimpleWeb::StatusCode::information_switching_protocols;
        };
}

void DirSyncServer::run() {
    std::promise<unsigned short> server_port;
    std::thread server_thread([this, &server_port]() {
        this->server->start([&server_port](unsigned short port) {
            server_port.set_value(port);
        });
    });
    spdlog::info("[DirSyncServer::run] Server thread started.");
    server_thread.join();
}
