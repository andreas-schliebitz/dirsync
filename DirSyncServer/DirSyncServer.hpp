#ifndef DIRSYNCSERVER_HPP
#define DIRSYNCSERVER_HPP

#include <string>
#include <memory>
#include <future>
#include <unordered_map>

#include "Using.hpp"
#include "FileManager.hpp"

class DirSyncServer {
  public:
    DirSyncServer(std::uint16_t port, const std::string &store,
                  const std::string &crtPath, const std::string &keyPath,
                  const std::string &endpoint);
    // Getter
    const std::filesystem::path &getStore() const;
    const std::filesystem::path &getCrtPath() const;
    const std::filesystem::path &getKeyPath() const;
    std::uint16_t getPort() const;

    // Setter
    void setStore(const std::filesystem::path &store);
    void setCrtPath(const std::filesystem::path &crtPath);
    void setKeyPath(const std::filesystem::path &keyPath);
    void setPort(std::uint16_t port);

    // Entry Point
    void run();

  private:
    std::uint16_t port;
    std::filesystem::path store;
    std::filesystem::path crtPath;
    std::filesystem::path keyPath;
    std::string endpoint;

    std::unique_ptr<WssServer> server;
    std::unordered_map<std::string, std::string> connections;

    void handleStringMessage(const std::string &strMsg);
    void handleBinaryMessage(const std::vector<std::uint8_t> &fileData) const;
    void handleRegistration(const json &jsonMsg);
    void writeFileData(const File &file) const;
    void distributeFileData(const std::string &machineId,
                            const std::vector<std::uint8_t> &bsonData) const;

    void onMessage();
    void onOpen();
    void onClose();
    void onError();
    void onHandshake();
};

#endif /* DIRSYNCSERVER_HPP */
