#include <cstdlib>
#include "main.hpp"
#include "DirSyncServer.hpp"
#include "getopt_pp.h"
#include <iostream>

int main(int argc, char **argv) {
    DirSyncServer server = createServer(argc, argv);
    server.run();

    return EXIT_SUCCESS;
}

DirSyncServer createServer(int argc, char **argv) {
    GetOpt::GetOpt_pp ops(argc, argv);

    bool help;
    ops >> GetOpt::OptionPresent('h', "help", help);
    if (help) {
        printUsage();
        std::exit(EXIT_SUCCESS);
    }

    std::string store, crtPath, keyPath, endpoint;
    std::uint16_t port = 8080;

    const std::string pwd = std::filesystem::current_path().native();

    ops >> GetOpt::Option('f', "store", store, pwd + "/received");
    ops >> GetOpt::Option('c', "crtPath", crtPath, pwd + "/server.crt");
    ops >> GetOpt::Option('k', "keyPath", keyPath, pwd + "/server.key");
    ops >> GetOpt::Option('p', "port", port);
    ops >> GetOpt::Option('e', "endpoint", endpoint, "^/file/?$");

    return DirSyncServer(port, store, crtPath, keyPath, endpoint);
}

void printUsage() {
    std::cout
        << "Usage:\n\t./dirsyncserver -h|--help\n\t./dirsyncserver "
           "-p|--port -e|--endpoint -f|--store -c|--crtPath "
           "-k|--keyPath\n\t\t-p|--port: Server port.\n\t\t-e|--endpoint: "
           "Server endpoint\n\t\t-f|--store: "
           "Filepath to store data.\n\t\t-c|--crtPath: Path to server "
           "certificate.\n\t\t-k|--keyPath Path to server key."
        << '\n';
}
