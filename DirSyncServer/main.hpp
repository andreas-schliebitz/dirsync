#ifndef MAIN_HPP
#define MAIN_HPP

#include "DirSyncServer.hpp"

DirSyncServer createServer(int argc, char **argv);
void printUsage();

#endif /* MAIN_HPP */
