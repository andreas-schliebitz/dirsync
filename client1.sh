#!/usr/bin/env bash

# Client 1 config
root=~/synced
host=192.168.0.39
local=192.168.0.39
endpoint=/file

FLAGS="--root $root --host $host --local $local --endpoint $endpoint"
echo "$FLAGS"

mkdir -p "$root"

sh -c "./DirSyncClient/dist/Release/GNU-Linux/dirsyncclient ${FLAGS}"
