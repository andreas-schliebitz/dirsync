#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>

#include "DirSyncClient.hpp"

DirSyncClient::DirSyncClient(const std::filesystem::path &root,
                             const std::string &host,
                             const std::string &localHost, std::uint16_t port,
                             const std::string &endpoint, bool verifyCert)
    : root(root), host(host), localHost(localHost), port(port),
      endpoint(endpoint), verifyCert(verifyCert),
      machineId(machineid::machineHash()), db(root, this->machineId) {
    const std::string conString = host + ":" + std::to_string(port) + endpoint;
    spdlog::info(
        "[DirSyncClient::DirSyncClient] Creating DirSync client with "
        "connection string \"{}\", local IP \"{}\" and machineId \"{}\".",
        conString, localHost, this->machineId);

    this->client =
        std::unique_ptr<WssClient>(new WssClient(conString, verifyCert));

    this->onOpen();
    this->onClose();
    this->onError();

    this->start();
}

const std::filesystem::path &DirSyncClient::getRoot() const {
    return this->root;
}

const std::string &DirSyncClient::getHost() const { return this->host; }

const std::string &DirSyncClient::getLocalHost() const {
    return this->localHost;
}

std::uint16_t DirSyncClient::getPort() const { return this->port; }

const std::string &DirSyncClient::getEndpoint() const { return this->endpoint; }

bool DirSyncClient::getVerifyCert() const { return this->verifyCert; }

void DirSyncClient::setHost(const std::string &host) { this->host = host; }

void DirSyncClient::setPort(std::uint16_t port) { this->port = port; }

void DirSyncClient::setEndpoint(const std::string &endpoint) {
    this->endpoint = endpoint;
}

void DirSyncClient::setVerifyCert(bool verifyCert) {
    this->verifyCert = verifyCert;
}

void DirSyncClient::handleOutgoingModifiedFile(const CltConnectionPtr &con,
                                               File &file) {
    if (this->db.contains(file)) {
        spdlog::info(
            "[DirSyncClient::handleOutgoingModifiedFile] Local file \"{}\" is "
            "tracked by client.",
            file.native());
        file.addVersionVector(
            this->db.at(file.native()).getVersionVector().data());
    }

    // (2) Local replica changed, increment its version vector entry.
    file.incrementVersionVector(this->machineId);

    spdlog::info("[DirSyncClient::handleOutgoingModifiedFile] Local file "
                 "\"{}\" was MODIFIED.\n"
                 "\tIncrementing version vector for this machine ({}): {}",
                 file.native(), this->machineId,
                 file.getVersionVector().at(this->machineId));

    this->db.insert(file.native(), file);

    std::filesystem::path src = this->root;
    src += this->modifiedFilepath;

    file.addData(FileManager::readBinary(src));
    this->sendFile(con, file);
}

void DirSyncClient::handleOutgoingCreatedFile(const CltConnectionPtr &con,
                                              File &file) {
    file.addVersionVectorEntry(this->machineId);

    spdlog::info("[DirSyncClient::handleOutgoingCreatedFile] Local file \"{}\" "
                 "was CREATED.\n"
                 "\tVersion vector for this machine ({}) is zero: {}",
                 file.native(), this->machineId,
                 file.getVersionVector().at(this->machineId));

    this->db.insert(file.native(), file);

    std::filesystem::path src = this->root;
    src += this->modifiedFilepath;

    file.addData(FileManager::readBinary(src));
    this->sendFile(con, file);
}

void DirSyncClient::handleOutgoingErasedFile(const CltConnectionPtr &con,
                                             const File &file) {
    spdlog::info("[DirSyncClient::handleOutgoingErasedFile] Local file \"{}\" "
                 "was ERASED.\n"
                 "\tDeletion command will be propagated to all connected "
                 "devices.",
                 file.native());
    if (this->db.contains(file)) {
        this->db.erase(file.native());
        this->sendFile(con, file);
    }
}

void DirSyncClient::sendRegistration(const CltConnectionPtr &con) const {
    const json registration = {
        {"type", "registration"},     {"machineId", this->machineId},
        {"ip", this->localHost},      {"port", this->port + 1},
        {"endpoint", this->endpoint},
    };
    spdlog::info("[DirSyncClient::sendFile] Sending registration: {}",
                 registration.dump());
    con->send(registration.dump());
}

void DirSyncClient::sendFile(const CltConnectionPtr &con,
                             const File &file) const {
    json fileJson = file.toJSON();
    fileJson["machineId"] = this->machineId;
    const auto fileBson = json::to_bson(fileJson);
    spdlog::info("[DirSyncClient::sendFile] Sending file \"{}\": {}",
                 file.native(), fileJson.dump());
    con->send(fileBson);
}

void DirSyncClient::onOpen() {
    this->client->on_open = [this](CltConnectionPtr con) {
        spdlog::info("[DirSyncClient::onOpen] Connection to server opened.");

        if (this->modifiedFilepath.empty()) {
            this->sendRegistration(con);
            con->send_close(1000, "Client " + this->machineId +
                                      " sent registration.");
        } else {
            File file(this->modifiedFilepath);
            file.addStatus(this->modifiedFileStatus);

            switch (this->modifiedFileStatus) {
            case FileStatus::MODIFIED: {
                this->handleOutgoingModifiedFile(con, file);
                break;
            }
            case FileStatus::CREATED: {
                this->handleOutgoingCreatedFile(con, file);
                break;
            }
            case FileStatus::ERASED:
                this->handleOutgoingErasedFile(con, file);
                break;
            default:
                // Never reached
                break;
            }
            con->send_close(1000, "Client " + this->machineId +
                                      " handled outgoing file action of type " +
                                      std::to_string(static_cast<int>(
                                          this->modifiedFileStatus)) +
                                      ".");
        }
    };
}

void DirSyncClient::onClose() {
    this->client->on_close = []([[maybe_unused]] CltConnectionPtr con,
                                int status, const std::string &reason) {
        spdlog::info(
            "[DirSyncClient::onClose] Connection to server closed with status "
            "code {}.\n\tReason: {}",
            status, reason);
    };
}

void DirSyncClient::onError() {
    this->client->on_error = []([[maybe_unused]] CltConnectionPtr con,
                                const ErrorCode &ec) {
        spdlog::error("[DirSyncClient::onError] Error {}.\n\tError message: {}",
                      ec.value(), ec.message());
    };
}

void DirSyncClient::start() {
    spdlog::info("[DirSyncClient::start] Starting client.");
    this->client->start();
}

void DirSyncClient::stop() {
    spdlog::info(
        "[DirSyncClient::stop] Stopping client and closing connection.");
    this->client->stop();
}

void DirSyncClient::cycle() {
    this->client->stop();
    this->client->start();
}

void DirSyncClient::run(const FileStatus &modifiedFileStatus,
                        const std::filesystem::path &modifiedFilepath) {
    spdlog::info("[DirSyncClient::start] Starting client.");
    this->modifiedFileStatus = modifiedFileStatus;
    this->modifiedFilepath = modifiedFilepath;
    this->cycle();
}
