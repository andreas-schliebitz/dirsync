#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>

#include "main.hpp"
#include "getopt_pp.h"
#include <iostream>
#include <filesystem>

int main(int argc, char **argv) {
    GetOpt::GetOpt_pp ops(argc, argv);

    bool help;
    ops >> GetOpt::OptionPresent('h', "help", help);
    if (help) {
        printUsage();
        std::exit(EXIT_SUCCESS);
    }

    std::string root, host, localHost, endpoint;
    std::uint16_t port = 8080;
    bool verifyCert = false;

    ops >> GetOpt::Option('r', "root", root,
                          std::filesystem::current_path().native() + "/synced");

    std::filesystem::path fsRoot(root);
    if (fsRoot.is_relative()) {
        spdlog::error("[main] Provided root path \"{}\" has to be absolute.",
                      root);
        std::exit(EXIT_FAILURE);
    }

    ops >> GetOpt::Option('s', "host", host, "127.0.0.1");
    ops >> GetOpt::Option('l', "local", localHost, "127.0.0.1");
    ops >> GetOpt::Option('p', "port", port);
    ops >> GetOpt::Option('e', "endpoint", endpoint, "/");
    ops >> GetOpt::OptionPresent('v', "verify", verifyCert);

    DirSyncClient client(fsRoot, host, localHost, port, endpoint, verifyCert);
    FilesystemWatchdog watchdog(client);
    watchdog.monitor();

    return EXIT_SUCCESS;
}

void printUsage() {
    std::cout
        << "Usage:\n\t./dirsyncclient -h|--help\n\t./dirsyncclient "
           "-r|--root -s|--host -l|--local -p|--port -e|--endpoint "
           "-v|--verify\n\t\t-r|--root: Filesystem path to "
           "sync.\n\t\t-s|--host: IP "
           "address of DirSyncServer.\n\t\t-l|--local: Local IP "
           "address.\n\t\t-p|--port: Port of "
           "DirSyncServer.\n\t\t-e|--endpoint: Endpoint the DirSyncServer is "
           "listening on.\n\t\t-v|--verify: Verify DirSyncServer's TLS "
           "certificate for secure WebSocket connection."
        << '\n';
}
