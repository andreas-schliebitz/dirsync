#define SPDLOG_FMT_EXTERNAL
#include <spdlog/spdlog.h>
#include <chrono>
#include <algorithm>
#include "FilesystemWatchdog.hpp"

FilesystemWatchdog::FilesystemWatchdog(DirSyncClient &client)
    : client(client), root(client.getRoot()),
      fileWatcher(client.getRoot(), std::chrono::milliseconds(1000)) {}

void FilesystemWatchdog::monitor() {
    spdlog::info("[FilesystemWatchdog::monitor] Monitoring \"{}\" for changes.",
                 this->client.getRoot().native());
    this->fileWatcher.start([this](std::string watchedPath,
                                   FileStatus status) -> void {
        std::filesystem::path watched(watchedPath);

        if ((!std::filesystem::is_regular_file(watched) &&
             status != FileStatus::ERASED)) {
            return;
        }

        if (FileManager::isReservedFile(this->root, watched) ||
            this->isLoop(watched)) {
            return;
        }

        spdlog::info(
            "[FilesystemWatchdog::start] File \"{}\" inside \"{}\" changed.",
            watchedPath, this->client.getRoot().native());

        switch (status) {
        case FileStatus::CREATED:
        case FileStatus::MODIFIED:
        case FileStatus::ERASED: {
            const auto relativeToRoot =
                FileManager::relativize(this->root, watched);
            spdlog::info("[FilesystemWatchdog::start] Notifying client about "
                         "status change {} of \"{}\".",
                         static_cast<int>(status), relativeToRoot.native());
            this->client.run(status, relativeToRoot);
            break;
        }
        default:
            spdlog::error("[FilesystemWatchdog::start] Unsupported "
                          "action on file \"{}\".",
                          watchedPath);
            break;
        }
    });
}

bool FilesystemWatchdog::isLoop(const std::filesystem::path &watched) const {
    std::filesystem::path lastReceivedSrc = this->root;
    lastReceivedSrc /= LAST_RECEIVED;
    const std::string lastReceivedFilepath =
        FileManager::readString(lastReceivedSrc);
    const auto relativeToRoot = FileManager::relativize(this->root, watched);
    if (lastReceivedFilepath == relativeToRoot) {
        FileManager::createFile(lastReceivedSrc); // truncate
        return true;
    }
    return false;
}
