#ifndef DIRSYNCCLIENT_HPP
#define DIRSYNCCLIENT_HPP

#include <string>
#include <memory>
#include <filesystem>
#include <vector>

#include "Using.hpp"
#include "File.hpp"
#include "FileManager.hpp"
#include "FileStatus.hpp"
#include "machineid.h"
#include "SyncDatabase.hpp"

class DirSyncClient {
  public:
    DirSyncClient(const std::filesystem::path &root,
                  const std::string &host = "127.0.0.1",
                  const std::string &localHost = "127.0.0.1",
                  std::uint16_t port = 8080,
                  const std::string &endpoint = "/", bool verifyCert = false);

    // Getter
    const std::filesystem::path &getRoot() const;
    const std::string &getHost() const;
    const std::string &getLocalHost() const;
    std::uint16_t getPort() const;
    const std::string &getEndpoint() const;
    bool getVerifyCert() const;

    // Setter
    void setHost(const std::string &host);
    void setLocalHost(const std::string &localHost);
    void setPort(std::uint16_t port);
    void setEndpoint(const std::string &endpoint);
    void setVerifyCert(bool verifyCert);

    void start();
    void stop();
    void cycle();

    void run(const FileStatus &fileStatus,
             const std::filesystem::path &modified);

  private:
    std::filesystem::path root;
    std::string host;
    std::string localHost;
    std::uint16_t port;
    std::string endpoint;
    bool verifyCert;
    std::string machineId;
    SyncDatabase db;

    std::unique_ptr<WssClient> client;

    std::filesystem::path modifiedFilepath;
    FileStatus modifiedFileStatus;

    // Listener
    void onOpen();
    void onClose();
    void onError();

    // Outgoing file handlers
    void handleOutgoingModifiedFile(const CltConnectionPtr &con, File &file);
    void handleOutgoingCreatedFile(const CltConnectionPtr &con, File &file);
    void handleOutgoingErasedFile(const CltConnectionPtr &con,
                                  const File &file);

    // Sending
    void sendRegistration(const CltConnectionPtr &con) const;
    void sendFile(const CltConnectionPtr &con, const File &file) const;
};

#endif /* DIRSYNCCLIENT_HPP */
