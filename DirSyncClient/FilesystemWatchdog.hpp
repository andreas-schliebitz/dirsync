#ifndef FILESYSTEMWATCHDOG_HPP
#define FILESYSTEMWATCHDOG_HPP

#include <filesystem>
#include <unordered_set>
#include <string>

#include "Using.hpp"
#include "FileManager.hpp"
#include "DirSyncClient.hpp"
#include "FileWatcher.hpp"

class FilesystemWatchdog {
  public:
    FilesystemWatchdog(DirSyncClient &client);
    void monitor();

  private:
    bool isLoop(const std::filesystem::path &watched) const;

    DirSyncClient &client;
    std::filesystem::path root;
    FileWatcher fileWatcher;
};

#endif /* FILESYSTEMWATCHDOG_HPP */
