#!/usr/bin/env bash

store=~/received 
port=8080
crtPath=./common/server.crt
keyPath=./common/server.key

FLAGS="--store $store --port $port --crtPath $crtPath --keyPath $keyPath"
echo "$FLAGS"

mkdir -p "$store"

sh -c "./DirSyncServer/dist/Release/GNU-Linux/dirsyncserver ${FLAGS}"
