#!/usr/bin/env bash

# ClientServer 2 config
root=~/synced
port=8081
crtPath=./common/server.crt
keyPath=./common/server.key

FLAGS="--root $root --port $port --crtPath $crtPath --keyPath $keyPath"
echo "$FLAGS"

mkdir -p "$root"

sh -c "./DirSyncClientServer/dist/Release/GNU-Linux/dirsyncclientserver ${FLAGS}"
