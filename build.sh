#!/usr/bin/env bash

cd ./DirSyncClient/
./release.sh
cd ..

cd ./DirSyncClientServer/
./release.sh
cd ..

cd ./DirSyncServer/
./release.sh
cd ..
